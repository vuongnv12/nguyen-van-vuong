#ifndef _Library_H_
#define _Library_H_

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define CONVERT(x) (x >>24)|((x & 0xFF00000)>>8)|((x & 0xFF00)<<8)|((x & 0xFF)<<24)

/*******************************************************************************
 * API
 ******************************************************************************/
/*!
 * @brief <Mo ta chuc nang cua ham>
 *
 * @param var3 <Mo ta muc dich cua var3>.
 * @param var4 <Mo ta muc dich cua var4>.
 *
 * @return <Mo ta cac gia tri tra ve>.
 */
int Dislay(uint32_t num);

#endif /* _Library_H_ */
